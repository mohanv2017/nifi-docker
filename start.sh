#!/bin/bash 

#    Licensed to the Apache Software Foundation (ASF) under one or more
#    contributor license agreements.  See the NOTICE file distributed with
#    this work for additional information regarding copyright ownership.
#    The ASF licenses this file to You under the Apache License, Version 2.0
#    (the "License"); you may not use this file except in compliance with
#    the License.  You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.

scripts_dir='/opt/nifi/scripts'

[ -f "${scripts_dir}/common.sh" ] && . "${scripts_dir}/common.sh"

## Custom code to handle NIFI_* variable substitutions

# Establish baseline properties
TMP_VAR="${NIFI_WEB_HTTP_PORT:=8080}"
TMP_VAR="${NIFI_WEB_HTTP_HOST:=$HOSTNAME}"
TMP_VAR="${NIFI_REMOTE_INPUT_HOST:=$HOSTNAME}"
TMP_VAR="${NIFI_REMOTE_INPUT_SOCKET_PORT:=10000}"
TMP_VAR="${NIFI_REMOTE_INPUT_SECURE:=false}"
TMP_VAR="${NIFI_CLUSTER_NODE_ADDRESS:=$HOSTNAME}"

nifi_vars=$( set | grep NIFI_ | grep -v ^_ | cut -d "=" -f1)
for i in $nifi_vars;
do
	var_tr=${i//_/.}
	var_tr=$(echo $var_tr | tr '[:upper:]' '[:lower:]')

	echo Replacing ${var_tr} using \$$i with value ${!i}
	prop_replace "${var_tr}" "${!i}"

done

# Check if staging directory is mounted.  If its mounted then copy all nars to lib
ls /staging/*.nar > /dev/null 2>&1 && cp -v /staging/*.nar ${NIFI_HOME}/lib

# If using external ZK, then configure zookeeper
[ ! -z "$ZK_ID" ] && mkdir -p ${NIFI_HOME}/state/zookeeper/version-2 && echo $ZK_ID > ${NIFI_HOME}/state/zookeeper/myid

genZkServers() {
	zk_splits=$(echo $ZK_SERVERS | tr , '\n')
	count=1
	for zks in $zk_splits;
	do
		echo server.$count=$zks
		let count=count+1
	done
}
[ ! -z "$ZK_SERVERS" ] && sed -i -e 's/^server.1=/#&\n/' conf/zookeeper.properties && genZkServers >> conf/zookeeper.properties 
# end of custom code

# Check if we are secured or unsecured
case ${AUTH} in
    tls)
        echo 'Enabling Two-Way SSL user authentication'
        . "${scripts_dir}/secure.sh"
        ;;
    ldap)
        echo 'Enabling LDAP user authentication'
        # Reference ldap-provider in properties
        prop_replace 'nifi.security.user.login.identity.provider' 'ldap-provider'
        prop_replace 'nifi.security.needClientAuth' 'WANT'

        . "${scripts_dir}/secure.sh"
        . "${scripts_dir}/update_login_providers.sh"
        ;;
    *)
        if [ ! -z "${NIFI_WEB_PROXY_HOST}" ]; then
            echo 'NIFI_WEB_PROXY_HOST was set but NiFi is not configured to run in a secure mode.  Will not update nifi.web.proxy.host.'
        fi
        ;;
esac

# Continuously provide logs so that 'docker logs' can    produce them
tail -F "${NIFI_HOME}/logs/nifi-app.log" &
"${NIFI_HOME}/bin/nifi.sh" run &
nifi_pid="$!"

trap "echo Received trapped signal, beginning shutdown...;" KILL TERM HUP INT EXIT;

echo NiFi running with PID ${nifi_pid}.
wait ${nifi_pid}
