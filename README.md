**Nifi docker compose**

Apache Nifi comes with a built in zookeeper for clustering.  The downside to using this embedded zookeeper is that Nifi cannot be scaled dynamically.  Also its recommended to have only 3 or 5 zookeeper instances and not anything more or less.  The docker compose provides 3 instances of nifi running embedded zookeeper and also provides a way to mount the nars.  The loadbalancer provides a way to loabalance between the available nifi instances using HAPROXY.  Nifi ui is accessible to port 8080.


*Preferred approach would be to not use embedded zookeeper, but use a seperate zookeeper ensemble for it, but its additional maintenance for zookeeper but Nifi can be dynamically scaled.*

Another docker compose yml file is provided to run 3 instances of zookeeper and by default a single instance of nifi.  With this approach, nifi instance can be dynamically scaled up or down as and when required.  To run in this mode, docker-compose -f docker-compose-zk.yml up --scale nifi=3 

